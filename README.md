# publish-readme

This project compiles a readme.md into html and publishes it at 
https://serra1.gitlab.io/publish-readme.

## development

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
cmark README.md -o ./output/index.html
```

## diagramming

```mermaid
sequenceDiagram
    participant Alice
    participant Bob
    Alice ->> Bob: Congratulations
```